{ pkgs }: pkgs.fetchFromGitHub {
  owner = "adaedra";
  repo = "dotfiles";
  name = "adaedra-dotfiles";
  rev = "080df3063024fdd85e7b8df4708dc22d7ea38138";
  fetchSubmodules = true;
  sha256 = "150e7e703b34e4f0aa2bdb5c4d630630736d172da1a6eee74bac9cd481ef8667";
}
