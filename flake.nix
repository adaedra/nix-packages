{
  inputs = {
    nixpkgs.url = "nixpkgs";
  };

  outputs = { self, nixpkgs }: {
    packages = (builtins.listToAttrs (map
      (system: { name = system; value = { dotfiles = import ./dotfiles.nix { pkgs = import nixpkgs { inherit system; }; }; }; })
      [ "x86_64-linux" "x86_64-darwin" ]
    ));

    nixosModules.dotfiles = { pkgs, ... }: let
      dotfiles = import ./dotfiles.nix { inherit pkgs; };
    in {
      users.defaultUserShell = pkgs.zsh;
      programs.zsh = {
        enable = true;
        interactiveShellInit = ''
          source ${dotfiles}/zsh/init.sh
        '';
        promptInit = "";
      };

      programs.vim.package = with pkgs; (vimUtils.makeCustomizable vim).customize {
        vimrcConfig.customRC = ''
          set background=dark
          source ${dotfiles}/vim/init.vim
        '';
      };
      programs.vim.defaultEditor = true;
    };
  };
}
